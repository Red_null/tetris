import time
from pprint import pprint

from asciimatics.screen import Screen, Canvas

from field import init_field, show_field
from game import frame, spawn_block

FIELD_SIZE = (10, 15)


def demo(screen: Canvas):
    while True:
        to_display = show_field(frame(game))
        if screen:
            for row_idx, row in enumerate(to_display):
                screen.print_at(row, 0, row_idx)
            key = screen.get_key()
            if key in (ord('Q'), ord('q')):
                return

            screen.refresh()
        else:
            print('*' * 100)
            pprint(to_display)

        time.sleep(game['speed'])


if __name__ == '__main__':
    game = {
        'field': init_field(FIELD_SIZE),
        'field_size': FIELD_SIZE,
        'speed': 0.3
    }

    spawn_block(game)
    try:
        Screen.wrapper(demo)
    except Exception:
        demo(None)
