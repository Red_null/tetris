from const import DYNAMIC_BLOCK, STATIC_BLOCK, BLOCK_SYMBOL


def init_field(dimensions):
    field = []
    for _ in range(dimensions[1]):
        row = []
        for _ in range(dimensions[0]):
            row.append(0)
        field.append(row)

    return field


def show_field(field):
    display_strings = []
    for row in field:
        row_strings = []
        for col in row:
            row_strings.append(BLOCK_SYMBOL if col in (DYNAMIC_BLOCK, STATIC_BLOCK) else '_')
        display_strings.append(' '.join(row_strings))
    return display_strings
