from copy import deepcopy
from itertools import takewhile
from random import randint

from block import Block
from const import STATIC_BLOCK, DYNAMIC_BLOCK



def rotate_block(game, side):
    pass


def add_score(game, points):
    pass


def update_speed(game):
    pass


def frame(game):
    block = game['current_block']
    field = deepcopy(game['field'])
    positions = block.get_blocks_positions()

    for position in positions:
        field[position.y][position.x] = DYNAMIC_BLOCK

    if not update_game_state(game):
        block.y += 1
    else:
        spawn_block(game)

    return field


def spawn_block(game):
    block = Block()
    block.x = randint(0, game['field_size'][0] - block.width)
    block.y = 0
    game['current_block'] = block


def update_game_state(game):
    field = game['field']
    block = game['current_block']
    if check_block_position(game, block):
        positions = block.get_blocks_positions()
        for position in positions:
            field[position.y][position.x] = STATIC_BLOCK

        return True

    return False


def check_block_position(game, block: Block):
    if check_field_borders(game, block):
        return True
    if check_static_block_borders(game, block):
        return True

    return False


def check_field_borders(game, block: Block):
    check_positions = block.get_blocks_positions(y_offset=1)
    field_size = game['field_size']

    for position in check_positions:
        if _check_borders(field_size, position):
            return True

    return False


def check_static_block_borders(game, block: Block):
    check_positions = block.get_blocks_positions(y_offset=1)
    field = game['field']
    for position in check_positions:
        if field[position.y][position.x] == STATIC_BLOCK:
            return True

    return False


def _check_borders(field_size, position):
    return field_size[1] <= position.y or field_size[0] <= position.x <= 0
