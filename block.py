from collections import namedtuple
from copy import deepcopy
from random import choice
from typing import List

SQUARE = (
    (1, 1),
    (1, 1)
)

LINE = (
    (1,),
    (1,),
    (1,),
    (1,)
)

ZIGZAG = (
    (1, 1, 0),
    (0, 1, 1)
)

LSHAPE = (
    (1, 1),
    (0, 1),
    (0, 1)
)

Point = namedtuple('Point', 'x, y')


class Block:
    x, y = 0, 0  # position of the left upper block
    SHAPES = (SQUARE, LINE, ZIGZAG, LSHAPE)

    def __init__(self):
        self.shape = deepcopy(choice(self.SHAPES))
        self.width, self.height = len(self.shape[0]), len(self.shape)
        self.point = Point(self.x, self.y)

    def get_blocks_positions(self, x_offset=None, y_offset=None) -> List[Point]:
        x_offset = x_offset if x_offset else 0
        y_offset = y_offset if y_offset else 0
        return [
            Point(self.x + col_idx + x_offset, self.y + row_idx + y_offset)
            for row_idx, row in enumerate(self.shape)
            for col_idx, col in enumerate(row)
            if self.shape[row_idx][col_idx] == 1
        ]

    def __repr__(self):
        return f'[{self.x};{self.y}]; {self.width}:{self.height}'
